public class CsDialog.TransformBin : Adw.Bin {
    private Gsk.Transform? _transform;
    public Gsk.Transform? transform {
        get { return _transform; }
        set {
            if (value == transform)
                return;

            _transform = value;

            queue_allocate ();
        }
    }
    private double _origin_x = 0.5;
    public double origin_x {
        get { return _origin_x;}
        set {
            if (value == origin_x)
                return;

            _origin_x = value;

            queue_allocate ();
        }
    }

    private double _origin_y = 0.5;
    public double origin_y {
        get { return _origin_x;}
        set {
            if (value == origin_y)
                return;

            _origin_y = value;

            queue_allocate ();
        }
    }

    construct {
        layout_manager = new Gtk.CustomLayout (null, measure, allocate);
    }

    private new static void measure (Gtk.Widget widget, Gtk.Orientation orientation, int for_size, out int minimum, out int natural, out int minimum_baseline, out int natural_baseline) {
        var self = widget as TransformBin;

        if (self.child == null) {
            minimum = 0;
            natural = 0;
            minimum_baseline = -1;
            natural_baseline = -1;

            return;
        }

        self.child.measure (orientation, for_size, out minimum, out natural, out minimum_baseline, out natural_baseline);
    }

    private new static void allocate (Gtk.Widget widget, int width, int height, int baseline) {
        var self = widget as TransformBin;

        if (self.child == null)
            return;

        float ox = (float) (width * self.origin_x);
        float oy = (float) (height * self.origin_y);

        var t = new Gsk.Transform ().translate ({ox, oy }).transform (self.transform).translate ({ -ox, -oy });
        self.child.allocate (width, height, baseline, t);
    }
}
