
int main (string[] args) {
	var app = new Gtk.Application ("org.example.App", ApplicationFlags.FLAGS_NONE);

	app.startup.connect (() => {
		Adw.init ();

        var provider = new Gtk.CssProvider ();
        provider.load_from_resource ("org/example/App/style.css");
        Gtk.StyleContext.add_provider_for_display (
            Gdk.Display.get_default (),
            provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
	});

	app.activate.connect (() => {
		var win = app.active_window;
		if (win == null) {
			win = new CsDialog.Window (app);
		}
		win.present ();
	});

	return app.run (args);
}
