public class CsDialog.Gizmo : Gtk.Widget {
    public Gizmo (string css_name = "widget") {
        Object (css_name: css_name);
    }

    protected override void dispose () {
        var child = get_first_child ();

        while (child != null) {
            var c = child;
            child = child.get_next_sibling ();

            c.unparent ();
        }

        base.dispose ();
    }
}

[GtkTemplate (ui = "/org/example/App/dialog.ui")]
public class CsDialog.Dialog : Adw.Bin {
    public signal void response (Gtk.ResponseType response);

    [GtkChild]
    private unowned Gtk.Label label;
    [GtkChild]
    private unowned Gtk.Label secondary_label;
    [GtkChild]
    private unowned LayoutSqueezer action_area;

    static construct {
        set_css_name ("window");
    }

    construct {
        var layout = new Gtk.BoxLayout (Gtk.Orientation.VERTICAL);
        layout.homogeneous = true;
        action_area.append_layout_manager (layout);

        layout = new Gtk.BoxLayout (Gtk.Orientation.HORIZONTAL);
        layout.homogeneous = true;
        action_area.append_layout_manager (layout);

        label.label = "Do something?";
        secondary_label.label = """If you do something,
bad things might happen.""";
        secondary_label.show ();

        add_button ("_Cancel", Gtk.ResponseType.CANCEL);
        add_button ("_Do It", Gtk.ResponseType.OK);
        add_button ("_Cancel", Gtk.ResponseType.CANCEL);
        add_button ("_Do It", Gtk.ResponseType.OK);
    }

    public void add_button (string text, Gtk.ResponseType r) {
        var btn = new Gtk.Button.with_mnemonic (text);
        btn.clicked.connect (() => {
            response (r);
        });

        btn.set_parent (action_area);
    }
}

public class CsDialog.DialogHost : Adw.Bin {
    private Gizmo? dimming;
    private TransformBin? bin;

    private Animation? animation;
    private SpringAnimation? animation2;

    protected override void dispose () {
        if (dimming != null) {
            dimming.unparent ();
            dimming = null;
        }

        if (bin != null) {
            bin.unparent ();
            bin = null;
        }

        base.dispose ();
    }

    private void animate_dimming (bool show) {
        var opacity = dimming.opacity;

        if (animation != null)
            animation.stop ();

        animation = new Animation (this, opacity, show ? 1 : 0, 500);
        animation.notify["value"].connect (() => {
            dimming.opacity = animation.value;
        });
        animation.done.connect (() => {
            animation = null;

            if (!show) {
                dimming.unparent ();
                dimming = null;
            }
        });

        animation.start ();
    }

    private void animate_dialog (bool show) {
        double scale = show ? 0 : 1;

        if (animation2 != null) {
            scale = animation2.value;
            animation2.stop ();
        }

        animation2 = new SpringAnimation.with_damping_ratio (
            this,
            scale,           // from
            show ? 1 : 0,    // to
            show ? 10 : -10, // velocity
            show ? 0.75 : 1, // damping ratio
            1,               // mass
            200,             // stiffness
            0.001            // epsilon
        );
        animation2.notify["value"].connect (() => {
            float s = (float) animation2.value;
            bin.transform = new Gsk.Transform ().scale (s, s);
            bin.opacity = s.clamp (0, 1);
        });
        animation2.done.connect (() => {
            animation2 = null;
            bin.transform = null;

            if (!show) {
                bin.unparent ();
                bin = null;
            }
        });

        animation2.start ();
    }

    public void hide_dialog () {
        animate_dimming (false);
        animate_dialog (false);
    }

    public void show_dialog () {
        if (dimming != null || bin != null)
            return;

        dimming = new Gizmo ("dimming");
        dimming.layout_manager = new Gtk.BinLayout ();
        dimming.opacity = 0;
        dimming.set_parent (this);

        var dialog = new Dialog ();
        dialog.halign = Gtk.Align.CENTER;
        dialog.valign = Gtk.Align.CENTER;
        dialog.response.connect (() => {
            hide_dialog ();
        });

        bin = new TransformBin ();
        bin.transform = new Gsk.Transform ().scale (0, 0);
        bin.child = dialog;
        bin.set_parent (this);

        animate_dimming (true);
        animate_dialog (true);
    }
}
